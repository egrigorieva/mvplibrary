package thumbtack.egrigorieva.mvplibrary.presentation.main

import android.content.Intent
import rx.Subscription
import thumbtack.egrigorieva.mvplibrary.R
import thumbtack.egrigorieva.mvplibrary.domain.auth.AuthInteractor
import thumbtack.egrigorieva.mvplibrary.domain.auth.AuthInteractor.Companion.RC_SIGN_IN
import thumbtack.egrigorieva.mvplibrary.domain.auth.User
import thumbtack.egrigorieva.mvplibrary.presentation.common.BasePresenter

class MainPresenter(view: MainView, router: MainRouter, private val authInteractor: AuthInteractor)
    : BasePresenter<MainView, MainRouter>(view, router) {
    internal var user: User? = null

    private fun buildSignInSubscription(): Subscription? {
        return authInteractor.buildSignInObservable().subscribe {
            if (it)
                view.showSignInSuccessMessage()
            else
                view.showSignInFailMessage()
        }
    }

    private fun buildAuthStateSubscription(): Subscription? {
        return authInteractor.buildAuthStateObservable().subscribe {
            user = it
            if (!it.isAuth) {
                router.setErrorAuthView()
                view.fillDrawerWithUnauth()
                authInteractor.signIn()
            } else {
                view.fillDrawer(it)
                router.openLibrary(0)
            }
        }
    }

    override fun onCreate() {
        view.initView()
        subscription.add(buildAuthStateSubscription())
        subscription.add(buildSignInSubscription())
    }

    fun onOptionsItemSelected(id: Int) {
    }

    fun onNavigationItemSelected(id: Int) {
        when (id) {
            R.id.all_books -> {
                router.openLibrary(MainRouter.ALL_BOOKS)
            }
            R.id.my_books -> {
                router.openLibrary(MainRouter.MY_BOOKS)
            }
            R.id.wish_books -> {
                router.openLibrary(MainRouter.WISH_BOOKS)
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SIGN_IN) {
            authInteractor.checkAuthResult(data)
        }
    }

    fun onUnauthConfirm() {
        signOut()
    }

    fun onNavigationBarSelected() {
        if (user?.isAuth ?: false) {
            view.openUnauthDialog()
        } else {
            authInteractor.signIn()
        }
    }

    private fun signOut() {
        authInteractor.signOut().subscribe {
            if (it)
                view.showSignOutSuccessMessage()
            else
                view.showSignOutFailMessage()
        }
    }

}
