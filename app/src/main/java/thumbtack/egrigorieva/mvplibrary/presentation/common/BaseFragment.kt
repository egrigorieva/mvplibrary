package thumbtack.egrigorieva.mvplibrary.presentation.common

import android.app.Fragment
import android.os.Bundle
import android.view.View

abstract class BaseFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

//    override fun onStart() {
//        super.onStart()
//        presenter.onStart()
//    }
//
//    override fun onStop() {
//        super.onStop()
//        presenter.onStop()
//    }

    protected abstract val presenter: BasePresenter<*, *>

}
