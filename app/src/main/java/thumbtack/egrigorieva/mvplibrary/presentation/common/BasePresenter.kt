package thumbtack.egrigorieva.mvplibrary.presentation.common

import rx.subscriptions.CompositeSubscription

abstract class BasePresenter<View : BaseView, Router>(var view: View, var router: Router) {
    protected val subscription = CompositeSubscription()


    open fun onStart() {
    }

    open fun onStop() {
    }

    open fun onCreate() {
    }

    open fun onDestroy() {
        subscription.clear()
    }
}


