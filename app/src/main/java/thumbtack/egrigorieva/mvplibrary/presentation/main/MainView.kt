package thumbtack.egrigorieva.mvplibrary.presentation.main

import thumbtack.egrigorieva.mvplibrary.domain.auth.User
import thumbtack.egrigorieva.mvplibrary.presentation.common.BaseView

interface MainView : BaseView {

    fun initView()
    fun fillDrawer(user: User)
    fun openUnauthDialog()
    fun fillDrawerWithUnauth()
    fun showSignOutSuccessMessage()
    fun showSignOutFailMessage()
    fun showSignInSuccessMessage()
    fun showSignInFailMessage()
}