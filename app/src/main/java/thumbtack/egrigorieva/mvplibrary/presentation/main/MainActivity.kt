package thumbtack.egrigorieva.mvplibrary.presentation.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import org.jetbrains.anko.*
import thumbtack.egrigorieva.mvplibrary.R
import thumbtack.egrigorieva.mvplibrary.domain.auth.AuthInteractorImpl
import thumbtack.egrigorieva.mvplibrary.domain.auth.User
import thumbtack.egrigorieva.mvplibrary.presentation.common.BaseActivity

class MainActivity : BaseActivity() {
    val mainPresenter: MainPresenter
        get() = presenter as MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = MainPresenter(MainViewImpl(), MainRouterImpl(), AuthInteractorImpl(this))
        mainPresenter.onCreate()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
            //TODO vDrawerLayout
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        mainPresenter.onOptionsItemSelected(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mainPresenter.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }


    inner class MainViewImpl : MainView {
        override fun initView() {
            setContentView(R.layout.activity_main)
            setSupportActionBar(toolbar)
            val toggle = ActionBarDrawerToggle(
                    this@MainActivity, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
            drawer_layout.addDrawerListener(toggle)
            toggle.syncState()
            initNavigationView()
        }

        private fun initNavigationView() {
            nav_view.setNavigationItemSelectedListener({ item ->
                mainPresenter.onNavigationItemSelected(item.itemId)
                drawer_layout.closeDrawer(GravityCompat.START)
                true
            })
            nav_view.getHeaderView(0).setOnClickListener({ mainPresenter.onNavigationBarSelected() })
            nav_view.inflateMenu(R.menu.activity_main_drawer)
        }

        override fun fillDrawer(user: User) {
            fillDrawer(user.email!!, user.name!!, user.thumbUrl)
        }

        override fun fillDrawerWithUnauth() {
            fillDrawer("", getString(R.string.click_to_authorize), null)
        }

        private fun fillDrawer(email: String, userName: String, photoUrl: String?) {
            Picasso.with(this@MainActivity)
                    .load(photoUrl)
                    .resize(80, 80)
                    .placeholder(R.mipmap.ic_account_circle_white_48dp)
                    .into(user_photo)
            user_name.text = userName
            user_email.text = email
        }

        override fun openUnauthDialog() {
            alert(R.string.really_unauth) {
                positiveButton(R.string.yes) { mainPresenter.onUnauthConfirm() }
                negativeButton(R.string.no) { dismiss() }
            }.show()
        }

        override fun showSignOutSuccessMessage() {
            toast(R.string.signOutSuccess)
        }

        override fun showSignOutFailMessage() {
            toast(R.string.signOutFail)
        }

        override fun showSignInSuccessMessage() {
            toast(R.string.signInSuccess)
        }

        override fun showSignInFailMessage() {
            toast(R.string.signInFail)
        }

    }


    inner class MainRouterImpl : MainRouter {
        override fun openLibrary(libraryType: Int) {
        }

        override fun setErrorAuthView() {
            content_main.removeAllViews()
            content_main.addView(UI {
                textView {
                    textResource = R.string.not_authorized
                    textSize = 25f
                }
            }.view)
        }
    }


}
