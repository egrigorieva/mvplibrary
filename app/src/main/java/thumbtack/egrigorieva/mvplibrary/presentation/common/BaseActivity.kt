package thumbtack.egrigorieva.mvplibrary.presentation.common

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

//    override fun onStart() {
//        super.onStart()
//        presenter.onStart()
//    }
//
//    override fun onStop() {
//        super.onStop()
//        presenter.onStop()
//    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        presenter.onCreate()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    protected lateinit var presenter: BasePresenter<*, *>
}
