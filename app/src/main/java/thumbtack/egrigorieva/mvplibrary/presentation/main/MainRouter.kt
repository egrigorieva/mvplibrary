package thumbtack.egrigorieva.mvplibrary.presentation.main

interface MainRouter {
    companion object {
        const val ALL_BOOKS = 0
        const val MY_BOOKS = 1
        const val WISH_BOOKS = 2
        const val NEW_BOOKS = 3
        const val DELAYED_BOOKS = 4
    }
    
    fun openLibrary (libraryType: Int)
    fun setErrorAuthView()
}
