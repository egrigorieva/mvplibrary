package thumbtack.egrigorieva.mvplibrary.domain.auth

import android.content.Intent
import rx.Observable
import rx.Single


interface AuthInteractor {

    companion object {
        const val RC_SIGN_IN = 31
    }

    fun signIn()
    fun checkAuthResult(data: Intent?)
    fun signOut(): Single<Boolean>
    fun buildAuthStateObservable(): Observable<User>
    fun buildSignInObservable(): Observable<Boolean>
}