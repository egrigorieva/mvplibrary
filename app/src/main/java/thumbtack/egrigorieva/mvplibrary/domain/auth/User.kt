package thumbtack.egrigorieva.mvplibrary.domain.auth

import com.google.firebase.auth.FirebaseUser

data class User(val name: String?, val email: String?, val thumbUrl: String?) {
    var isAuth = false
        private set

    init {
        if (name != null)
            isAuth = true
    }

    constructor(user: FirebaseUser?) :
            this(user?.displayName, user?.email, user?.photoUrl.toString())

}