package thumbtack.egrigorieva.mvplibrary.domain.auth

import android.content.Intent
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import rx.Observable
import rx.Single
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import thumbtack.egrigorieva.mvplibrary.R
import thumbtack.egrigorieva.mvplibrary.domain.auth.AuthInteractor.Companion.RC_SIGN_IN

class AuthInteractorImpl(val activity: AppCompatActivity) : AuthInteractor {
    private var googleApiClient: GoogleApiClient
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private lateinit var signInSubscriber: Subscriber<in Boolean>

    init {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        googleApiClient = GoogleApiClient.Builder(activity)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
        googleApiClient.connect()
    }

    override fun buildAuthStateObservable(): Observable<User> {
        return Observable.create<User> { subscriber ->
            auth.addAuthStateListener {
                subscriber.onNext(User(it.currentUser))
            }
        }.subscribeOn(AndroidSchedulers.mainThread())
    }

    override fun buildSignInObservable(): Observable<Boolean> {
        return Observable.create<Boolean> { signInSubscriber = it }
    }

    override fun signOut(): Single<Boolean> {
        return Single.create<Boolean> { subscriber ->
            auth.signOut()
            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback {
                subscriber.onSuccess(it.isSuccess)
            }
        }
    }

    override fun signIn() {
        val authorizeIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)
        ActivityCompat.startActivityForResult(activity, authorizeIntent, RC_SIGN_IN, null)
    }

    override fun checkAuthResult(data: Intent?) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
        if (result.isSuccess) {
            val account = result.signInAccount!!
            val credential = GoogleAuthProvider.getCredential(account.idToken, null)
            auth.signInWithCredential(credential)
            googleApiClient.connect()
        }
        signInSubscriber.onNext(result.isSuccess)
    }

}