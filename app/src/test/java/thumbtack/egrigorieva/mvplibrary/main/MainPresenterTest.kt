package thumbtack.egrigorieva.mvplibrary.main

import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import rx.Scheduler
import rx.android.plugins.RxAndroidPlugins
import rx.android.plugins.RxAndroidSchedulersHook
import rx.schedulers.Schedulers
import thumbtack.egrigorieva.mvplibrary.baseMock.MockIntent
import thumbtack.egrigorieva.mvplibrary.baseMock.MockRouter
import thumbtack.egrigorieva.mvplibrary.domain.auth.AuthInteractor
import thumbtack.egrigorieva.mvplibrary.domain.auth.User
import thumbtack.egrigorieva.mvplibrary.presentation.main.MainPresenter


class MainPresenterTest {
    var mockView = MockView()
    var mockRouter = MockRouter()
    var mockAuthInteractor = MockAuthInteractor(this)
    var presenter = MainPresenter(mockView, mockRouter, mockAuthInteractor)
    val testUser = User("name", "email", null)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        RxAndroidPlugins.getInstance().registerSchedulersHook(object : RxAndroidSchedulersHook() {
            override fun getMainThreadScheduler(): Scheduler {
                return Schedulers.immediate()
            }
        })
        mockView = MockView()
        mockRouter = MockRouter()
        mockAuthInteractor = MockAuthInteractor(this)
        presenter = MainPresenter(mockView, mockRouter, mockAuthInteractor)
    }

    @After
    fun tearDown() {
        RxAndroidPlugins.getInstance().reset()
    }

    private fun openNotAuthorize() {
        presenter.onCreate()
        presenter.onStart()
        mockAuthInteractor.callAuthListener(User(null))
    }

    private fun openAuthorize() {
        presenter.onCreate()
        presenter.onStart()
        mockAuthInteractor.callAuthListener(testUser)
    }

    @Test
    fun testOpenNotAuthorize() {
        openNotAuthorize()
        assertEquals(1, mockView.initCount)
        assertEquals(1, mockRouter.setErrorAuthViewCount)
        assertEquals(1, mockView.fillDrawerWithUnauthCount)
        assertEquals(1, mockAuthInteractor.signInCount)
        assertEquals(User(null), presenter.user)
    }

    @Test
    fun testOpenAuthorize() {
        openAuthorize()
        assertEquals(1, mockView.initCount)
        assertEquals(1, mockRouter.openLibraryCount)
        assertEquals(1, mockView.fillDrawerCounter.size)
        assertEquals(testUser, mockView.fillDrawerCounter[0])
        assertEquals(testUser, presenter.user)

    }

    @Test
    fun signInSuccessTest() {
        openNotAuthorize()
        presenter.onNavigationBarSelected()
        assertEquals(2, mockAuthInteractor.signInCount)
        val i = MockIntent()
        i.putExtra("isAuth", true)
        presenter.onActivityResult(AuthInteractor.RC_SIGN_IN, 0, i)

        assertEquals(1, mockRouter.openLibraryCount)
        assertEquals(1, mockView.fillDrawerCounter.size)
        assertEquals(1, mockView.showSignInSuccessMessageCount)
        assertEquals(testUser, mockView.fillDrawerCounter[0])
        assertEquals(testUser, presenter.user)
    }

    @Test
    fun signInFailTest() {
        openNotAuthorize()
        presenter.onNavigationBarSelected()
        assertEquals(2, mockAuthInteractor.signInCount)
        val i = MockIntent()
        i.putExtra("isAuth", false)
        presenter.onActivityResult(AuthInteractor.RC_SIGN_IN, 0, i)

        assertEquals(0, mockRouter.openLibraryCount)
        assertEquals(0, mockView.fillDrawerCounter.size)
        assertEquals(1, mockView.showSignInFailMessageCount)
        assertEquals(User(null), presenter.user)
    }

    @Test
    fun signOutTest() {
        openAuthorize()
        presenter.onNavigationBarSelected()
        assertEquals(1, mockView.openUnauthDialogCount)
        presenter.onUnauthConfirm()
        assertEquals(1, mockAuthInteractor.signOutCount)

        assertEquals(1, mockRouter.setErrorAuthViewCount)
        assertEquals(1, mockView.fillDrawerWithUnauthCount)
        assertEquals(1, mockView.showSignOutSuccessMessageCount)
        assertEquals(1, mockAuthInteractor.signInCount)
        assertEquals(User(null), presenter.user)
    }

    @Test
    fun forcedDisconnectTest() {
        openAuthorize()
        mockAuthInteractor.callAuthListener(User(null))

        assertEquals(1, mockRouter.setErrorAuthViewCount)
        assertEquals(1, mockView.fillDrawerWithUnauthCount)
        assertEquals(1, mockAuthInteractor.signInCount)
        assertEquals(User(null), presenter.user)
    }

    @Test
    fun onRebuildInAuthWithSlowResultTest() {
        openNotAuthorize()
        presenter.onStop()

        mockView = MockView()
        mockRouter = MockRouter()
        mockAuthInteractor = MockAuthInteractor(this)
        presenter = MainPresenter(mockView, mockRouter, mockAuthInteractor)
        presenter.onCreate()
        presenter.onStart()

        val i = MockIntent()
        i.putExtra("isAuth", true)
        presenter.onActivityResult(AuthInteractor.RC_SIGN_IN, 0, i)

        assertEquals(1, mockRouter.openLibraryCount)
        assertEquals(1, mockView.fillDrawerCounter.size)
        assertEquals(testUser, mockView.fillDrawerCounter[0])
        assertEquals(testUser, presenter.user)
    }

    @Test
    fun onRebuildInAuthWithQuickResultTest() {
        openNotAuthorize()
        presenter.onStop()

        mockView = MockView()
        mockRouter = MockRouter()
        mockAuthInteractor = MockAuthInteractor(this)
        presenter = MainPresenter(mockView, mockRouter, mockAuthInteractor)
        presenter.onCreate()

        val i = MockIntent()
        i.putExtra("isAuth", true)
        presenter.onActivityResult(AuthInteractor.RC_SIGN_IN, 0, i)
        presenter.onStart()

        assertEquals(1, mockRouter.openLibraryCount)
        assertEquals(1, mockView.fillDrawerCounter.size)
        assertEquals(testUser, mockView.fillDrawerCounter[0])
        assertEquals(testUser, presenter.user)
    }


}