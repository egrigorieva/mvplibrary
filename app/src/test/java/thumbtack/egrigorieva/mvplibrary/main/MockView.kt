package thumbtack.egrigorieva.mvplibrary.main

import thumbtack.egrigorieva.mvplibrary.domain.auth.User
import thumbtack.egrigorieva.mvplibrary.presentation.main.MainView
import java.util.*

class MockView : MainView {
    var initCount: Int = 0
    var fillDrawerCounter: MutableList<User> = ArrayList()
    var openUnauthDialogCount: Int = 0
    var fillDrawerWithUnauthCount: Int = 0
    var showSignOutSuccessMessageCount: Int = 0
    var showSignOutFailMessageCount: Int = 0
    var showSignInSuccessMessageCount: Int = 0
    var showSignInFailMessageCount: Int = 0

    override fun initView() {
        initCount++
    }

    override fun fillDrawer(user: User) {
        fillDrawerCounter.add(user)
    }

    override fun openUnauthDialog() {
        openUnauthDialogCount++
    }

    override fun fillDrawerWithUnauth() {
        fillDrawerWithUnauthCount++
    }

    override fun showSignOutSuccessMessage() {
        showSignOutSuccessMessageCount++
    }

    override fun showSignOutFailMessage() {
        showSignOutFailMessageCount++
    }

    override fun showSignInSuccessMessage() {
        showSignInSuccessMessageCount++
    }

    override fun showSignInFailMessage() {
        showSignInFailMessageCount++
    }

}