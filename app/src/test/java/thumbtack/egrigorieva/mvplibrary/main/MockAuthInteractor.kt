package thumbtack.egrigorieva.mvplibrary.main

import android.content.Intent
import rx.Observable
import rx.Single
import rx.Subscriber
import thumbtack.egrigorieva.mvplibrary.baseMock.MockIntent
import thumbtack.egrigorieva.mvplibrary.domain.auth.AuthInteractor
import thumbtack.egrigorieva.mvplibrary.domain.auth.User

class MockAuthInteractor(private val mainPresenterTest: MainPresenterTest) : AuthInteractor {

    var authStateSubscriber: Subscriber<in User>? = null
    var signInSubscriber: Subscriber<in Boolean>? = null

    var signInCount: Int = 0
    var checkAuthResultCount: Int = 0
    var signOutCount: Int = 0

    override fun signIn() {
        signInCount++
    }

    override fun buildAuthStateObservable(): Observable<User> {
        return Observable.create<User> { authStateSubscriber = it }
    }

    override fun buildSignInObservable(): Observable<Boolean> {
        return Observable.create<Boolean> { signInSubscriber = it }
    }

    override fun checkAuthResult(data: Intent?) {
        checkAuthResultCount++
        val i = data as MockIntent
        if (i.getBooleanExtra("isAuth", false)) {
            callAuthListener(mainPresenterTest.testUser)
            signInSubscriber!!.onNext(true)
        } else {
            signInSubscriber!!.onNext(false)
        }
    }

    override fun signOut(): Single<Boolean> {
        signOutCount++
        callAuthListener(User(null))
        return Single.create<Boolean> { it.onSuccess(true) }
    }

    fun callAuthListener(user: User) {
        authStateSubscriber!!.onNext(user)
    }

}