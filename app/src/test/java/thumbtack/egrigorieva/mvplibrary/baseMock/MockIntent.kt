package thumbtack.egrigorieva.mvplibrary.baseMock

import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import java.util.*

class MockIntent : Intent() {
    private var extras: MutableMap<String, Any>? = null

    override fun putExtra(name: String, value: String): Intent {
        if (extras == null) {
            extras = HashMap()
        }
        extras!!.put(name, value)
        return this
    }

    override fun putExtra(name: String, value: Boolean): Intent {
        if (extras == null) {
            extras = HashMap()
        }
        extras!!.put(name, value)
        return this
    }

    override fun getStringExtra(name: String) = extras?.get(name) as String

    override fun getBooleanExtra(name: String, defaultValue: Boolean) = (extras?.get(name) ?: defaultValue) as Boolean

    val CREATOR: Parcelable.Creator<Intent> = object : Parcelable.Creator<Intent> {
        override fun createFromParcel(`in`: Parcel): Intent {
            return Intent()
        }

        override fun newArray(size: Int): Array<Intent?> {
            return arrayOfNulls(size)
        }
    }
}