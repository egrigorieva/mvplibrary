package thumbtack.egrigorieva.mvplibrary.baseMock

import thumbtack.egrigorieva.mvplibrary.presentation.main.MainRouter

class MockRouter : MainRouter {
    var openLibraryCount: Int = 0
    var setErrorAuthViewCount: Int = 0

    override fun openLibrary(libraryType: Int) {
        openLibraryCount++
    }

    override fun setErrorAuthView() {
        setErrorAuthViewCount++
    }

}